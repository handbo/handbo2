class ApplicationMailer < ActionMailer::Base
  default from: "'Handbo' <from@example.com>"
  layout 'mailer'
end
