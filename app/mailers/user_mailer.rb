class UserMailer < ActionMailer::Base

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #

 default from: "info@handbo.id"

  def password_reset(user)
    @user = user
    @url = 'http://www.handbo.id/login'
    mail to: user.email, subject: "Password Reset"
  end

  def welcome_email(user)
      @user = user
    @url  = 'http://www.handbo.id/login'
    mail(:to => "#{user.email}", :subject => "Selamat Datang di Handbo")
  end

  def registration_confirmation(user)
   @user = user
    mail(:to => "#{user.email}", :subject => "Selamat Datang di Handbo")
 end

end

