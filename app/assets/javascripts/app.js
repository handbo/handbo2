var app = angular.module('myapp',['ngRoute']);

	app.config(['$stateProvider','$urlRouterProvider',
		function($stateProvider,$urlRouterProvider) {
			$stateProvider 
				.state('home',{
					url:'/home',
					templateUrl:'main/_home.html',
					controller:'MainCtrl'
				});
				$urlRouterProvider.otherwise('home');
		}])