var Book = React.createClass({
	getInitialState() {
		return {
			book: this.props.book,
			editMode:false,
			errors:{}

		}
	},

	setEditMode() {
		this.setState({editMode:true});
	},
	handleTitleChange(e) {
		var newBook = this.state.book
		newBook.title = e.target.value
		this.setState({book:newBook});
	},
	handleJudulChange(e){
		var newBook = this.state.book
		newBook.judul= e.target.value
		this.setState({book:newBook});
	},
	 handleBookUpdate() {
    var that = this;
    $.ajax({
      method: 'PUT',
      data: {
        book: that.state.book,
      },
      url: '/books/' + that.state.book.id + '.json',
      success: function(res) {
        that.setState({
          errors: {},
          book: res,
          editMode: false
        });
      },
      error: function(res) {
        that.setState({errors: res.responseJSON.errors});
      }
    });
  },
  handleBookFire() {
    var that = this;
    $.ajax({
      method: 'DELETE',
      url: '/books/' + that.state.book.id + '.json',
      success: function(res) {
        that.props.onFireBook(that.state.book);
      }
    })
  },

	render () {
		return (
			<tr>
				<td>{this.state.title}</td>
				<td>{this.state.judul}</td>
			</tr>
			);

	}
});