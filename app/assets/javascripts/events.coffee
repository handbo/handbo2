date_range_picker = undefined
date_range_picker = ->
  $('#fullcalendar').fullCalendar
    firstDay: 1
    lang: 'es'
    header:
      left: 'title'
      center: ''
      right: 'prev,next'
    eventSources: '/events.json'
  return

$(document).on 'turbolinks:load', date_range_picker
$('#calendar').fullcalendar()