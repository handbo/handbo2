class RekamanSerializer < ActiveModel::Serializer
  attributes :id, :title, :date, :amount
end
