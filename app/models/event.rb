class Event < ActiveRecord::Base
	validates :title, presence: true
  attr_accessor :date_range
def all_day_event?
    self.start == self.start.midnight && self.end == self.end.midnight ? true : false
  end

  def as_json(options = {})
    {
      :id => self.id,
      :title => self.title,
      :start => self.start,
      :end => self.end,
     }
  end
end
