class Sewa < ActiveRecord::Base
  belongs_to :user
  belongs_to :product

 	EMAIL_REGEX = /\b[A-Z0-9._%a-z\-]+@(?:[A-Z0-9a-z\-]+\.)+[A-Za-z]{2,4}\z/

  validates :tglawalsewa, presence: {message: "Belum Diisi!"}
  validates :tglakhirsewa, presence: {message: "Belum Diisi"}

  validates_format_of :email, :with => EMAIL_REGEX
  validates :email, presence: {message: "Tidak Boleh Kosong"}
  validates :nomorhp, presence: {message: "HP Belum Diisi"}

end
