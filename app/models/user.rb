class User < ActiveRecord::Base
   attr_accessor :remember_token, :activation_token, :reset_token
 
  has_many :products,dependent: :destroy
  has_many :sewas
  has_many :komentars
  has_many :transaksis
  has_many :purchases, dependent: :destroy
  has_many :products, through: :purchases
  has_many :reviews
  before_create :confirmation_token


  attr_accessor :password,:encrypted_password
  
  has_secure_password validations: false
  

  EMAIL_REGEX = /\b[A-Z0-9._%a-z\-]+@(?:[A-Z0-9a-z\-]+\.)+[A-Za-z]{2,4}\z/


  before_save :encrypt_password
  validates_confirmation_of :password
  validates_uniqueness_of :email,:message => "sudah terdaftar, silahkan masukan email yang lain."
  validates :password, :length => { :minimum => 6 , message: "terlalu pendek, minimal adalah 6 karakter."}, unless: Proc.new { |a| !a.new_record? && a.password.blank? }
  validates_presence_of :password, :on => :create
  validates_format_of :email, :with => EMAIL_REGEX
  validates_presence_of :email
  accepts_nested_attributes_for :products

  


  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user

    else
      nil
    end
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end


  def send_password_reset
    generate_token(:password_reset_token)
    self.password_reset_sent_at = Time.zone.now
    save!
    UserMailer.password_reset(self).deliver
  end

 def generate_token(column)

    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end

   # Sets the password reset attributes.
  def create_reset_digest
    #self.reset_token = User.new_token
    update_attribute(:reset_Digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # Sends password reset email.
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end


  private
  def email_activate
    self.email_confirmed = true
    self.confirm_token = nil
    save!(:validate => false)
  end

   def confirmation_token
     if self.confirm_token.blank?
         self.confirm_token = SecureRandom.urlsafe_base64.to_s
   end
  end

end

