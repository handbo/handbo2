class Product < ActiveRecord::Base
	has_attached_file :image, styles: { large: "600x600>", medium: "300x225>", thumb: "100x100>" }, default_url: "public/images"
  	validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
  	has_attached_file :image2, styles: { large: "600x600>", medium: "300x225>", thumb: "100x100>" }, default_url: "public/images"
  	validates_attachment_content_type :image2, content_type: /\Aimage\/.*\z/
    #mount_uploader :image, ImageUploader
  	#attr_accessor :nama, :item, :descriptions, :price, :from, :until, :stadt, :subitem, :assets_attributes
  	#has_many :assets
  	#accepts_nested_attributes_for :assets, allow_destroy: true, reject_if: lambda {|attributes| attributes['image'].blank?}
  	#attr_accessor :assets_attributes
  has_many :penawarans
	has_many :order_items
	belongs_to :users
	has_many :sewas	

  	has_many :sewas
	validates :description, :subitem, :stadt, :image, :image2, :item, :klasifikasikategory, :item, :tipe, :platnomor, :lepaskunci,:transmisi, presence: { message: "Tidak Boleh Kosong"}
	validates :harga6jam, :harga12jam, :hargafullday, 
				presence: true, 
				format: { with: /\A\d+(?:\.\d{0,2})?\z/ },
				numericality: { greater_than: 0, less_than: 100000000 }

	
	def self.search(search)
		#tire.search(load.true)  do
		#	query{string params[:query],default_operator:"AND"} if params[:query].present?
		#	filter :range,published_at:{lte: Time.zone.now}
		where("lower(item) LIKE? OR lower(stadt) LIKE?", "%#{search.downcase}%","%#{search.downcase}%")
	end
	
	def sewa
		self.tambahanproduk -=1
		self.save
		
	end
end
