class Home < ActiveRecord::Base
	belongs_to :user

	def self.search(search)
		where("lower(item) LIKE?", "%#{search}%",)
	end
end
