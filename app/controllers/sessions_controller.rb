class SessionsController < ApplicationController
  def new
  end



  def create
      if user = User.authenticate(params[:email], params[:password])
  if user
    user.email_confirmed
    session[:current_user_id] = user.id
    session[:user_id] = user.id
    redirect_to home_path, :notice => "Logged in!"
    else
        flash.now[:error] = 'Silahkan Aktivasi Email Anda Terlebih Dahulu!'
        render 'new'
      end
  else

    redirect_to login_path, danger: "Email Atau Password Yang Anda Masukan Salah"

  end
  end
  

  def destroy
    session[:current_user_id] = nil
    session[:user_id] = nil
    cookies.delete(:auth_token)
    redirect_to root_url, :notice => "Logged out!"
  end

end
