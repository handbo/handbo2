class PagesController < ApplicationController
before_action :logged_in_user,only:[:home]

  def home
  	@Users = User.all
  	@Products= Product.all
  end

  def blog
  end

  private

  def logged_in_user
  	unless logged_in?
  		flash[:danger] ="Please Log in"
  		redirect_to login_url
  	end
  	
  end
end
