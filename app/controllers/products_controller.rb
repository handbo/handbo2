class ProductsController < ApplicationController
  #before_action :set_product, only: [:sewaitem, :show, :edit, :update, :destroy,:coba,:myproduct]
  #before_action :find_user_object
  before_action :logged_in_user,only:[:update,:destroy,:show,:index,:new]
  before_action :set_product,only: [:show,:booked,:sewa]
  

  # GET /products
  # GET /products.json
  def index
   @user = current_user
   #@product = Product.all
   @products = @user.products.all
   if params[:search]
    @products = Product.search(params[:search]).order("created_at DESC").page(params[:page]).per_page(8)
    else
    @products = Product.all.order("created_at DESC").page(params[:page]).per_page(8)
    end
  end

  def home
   
   #@products = Product.reorder("created_at DESC").page(params[:page]).per_page(8)
   @user = current_user   
    @products = Product.all
    if params[:search]
    @products = Product.search(params[:search]).order("created_at DESC").page(params[:page]).per_page(8)
    else
    @products = Product.all.order("created_at DESC").page(params[:page]).per_page(8)
    end
  end
  

  # GET /products/1
  # GET /products/1.json
  def show
    @reviews = Review.all.order("created_at DESC")
  end

  def sewa
    redirect_to home_path,notice: "Anda telah nyewa #{@product.item}"
    

  end  

  def search
    @products = Product.all
  end
  
  def sewaitem
 
  end

  def lihat
    #@products = Product.all.paginate(page:params[:page],per_page:10)
    #@products = Product.all.reorder("created_at DESC").page(params[:page]).per_page(8)
      @products = Product.all
  if params[:search]
    @products = Product.search(params[:search]).order("created_at DESC").page(params[:page]).per_page(8)
  else
    @products = Product.all.order("created_at DESC").page(params[:page]).per_page(8)
  end
  end    


  # GET /products/new
  def new
    @product = Product.new

  end

  def validasi
    @products = Product.all
  end
  
  def penawaran
    
  end

# GET /products/1/edit
  def edit
    @product = Product.find(params[:id])
     @user = current_user
    products = current_user.products
    @users = @user.products
  end

  def booked
     
  end


  # POST /products
  # POST /products.json
  def create
   #@user = current_user
    #@product = Product.new(product_params)
    #@product = @user.products.new(product_params)
    @product = current_user.products.create(product_params)

    #if verify_recaptcha(model: @product) && @product.save 

    respond_to do |format|
      if @product.save

        format.html { redirect_to lihat_path,notice: 'Telah berhasil dibuat.' }
        format.json { render :json, status: :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
def update
    @product = Product.find(params[:id])
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :json, status: :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
   @product = Product.find(params[:id])
    products = current_user.purchases.find_by(product_id: params[:id]).destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to myproduct_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:item, :from,:image,:image2, :tipe, :klasifikasikategory, :platnomor, :stadt, :subitem, :lepaskunci,:transmisi,:current_user_id,:user_id,:product_id,:harga6jam,:booked,:harga12jam,:hargafullday,:description)
    end  

    def find_user_object
      current_user = User.find_by(params[:user_id])
    end

    def logged_in_user
    unless logged_in?
      flash[:danger] ="Please Log in"
      redirect_to login_url
    end    
  end
end
