class SewasController < ApplicationController
  before_action :set_sewa, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user,only:[:index]
  
  # GET /sewas
  # GET /sewas.json
  def index
    @sewas = Sewa.all
  end

  # GET /sewas/1
  # GET /sewas/1.json
  def show
  end

  # GET /sewas/new
  def new
    @sewa = Sewa.new
  
  end
  # GET /sewas/1/edit
  def edit
  end

  # POST /sewas
  # POST /sewas.json
  def create
  

    @sewa =current_user.sewas.new(sewa_params)
    

    respond_to do |format|
      if @sewa.save

        format.html { redirect_to @sewa, notice: 'Sewa was successfully created.' }
        format.json { render :json,status: :show, status: :created, location: @sewa }
      else
        format.html { render :new }
        format.json { render json: @sewa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sewas/1
  # PATCH/PUT /sewas/1.json
  def update
    respond_to do |format|
      if @sewa.update(sewa_params)
        format.html { redirect_to @sewa, notice: 'Sewa was successfully updated.' }
        format.json { render :show, status: :ok, location: @sewa }
      else
        format.html { render :edit }
        format.json { render json: @sewa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sewas/1
  # DELETE /sewas/1.json
  def destroy
    @sewa.destroy
    respond_to do |format|
      format.html { redirect_to sewas_url, notice: 'Sewa was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sewa
      @sewa = Sewa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sewa_params
      params.require(:sewa).permit(:tglawalsewa, :tglakhirsewa, :email, :nomorhp, :user_id, :product_id)
    end
    def logged_in_user
    unless logged_in?
      flash[:danger] ="Please Log in"
      redirect_to login_url
    end    
  end
end
