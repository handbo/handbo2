class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  helper_method :current_user
  include SessionsHelper




private
add_flash_types :danger, :info, :warning, :success

add_flash_types :danger, :info, :warning, :success

def current_user
  @current_user ||= session[:current_user_id] && User.find_by(id: session[:current_user_id])
end

def logged_in
  !current_user
end



def current_product
    if !session[:product_id].nil?
      Product.find(session[:product_id])
    else
      Product.new
    end
  end


end
