class WelcomeController < ApplicationController
  def index
  	#@products = Product.reorder("created_at DESC").page(params[:page]).per_page(8)
  	@products = Product.all
  	if params[:search]
    @products = Product.search(params[:search]).order("created_at DESC").page(params[:page]).per_page(8)
  	else
    @products = Product.all.order("created_at DESC").page(params[:page]).per_page(8)
  	end
  end

end
