class PasswordResetsController < ApplicationController
  def new
  end


 def create
      @user = User.find_by_email(params[:email])
      if @user
          @user.send_password_reset
          flash[:success] = "Silahkan cek email anda dan ikuti langkah-langkahnya" 
          redirect_to login_path
      else
          redirect_to password_resets_new_url, danger: "Email tidak di temukan"
      end
  end




  
def reset_password
    @user = current_website.users.find_by_password_reset_token!(params[:id])
  end
  

def edit
  @user = User.find_by_password_reset_token!(params[:id])
end




def update
  @user = User.find_by_password_reset_token!(params[:id])
  if @user.password_reset_sent_at < 2.hours.ago
    redirect_to new_password_reset_path, :alert => "Password &crarr; 
      reset has expired."
  elsif @user.update_attributes(params.require(:user).permit(:password, :password_confirmation))
    redirect_to login_path, :success => "Password berhasil di ubah."
  else
    render :edit
  end
end



end


