class ReviewsController < ApplicationController

before_action :find_product

def new
	@review = Review.new
end

def create
	@product = Product.find(params[:product_id])
	@review = @product.reviews.create(review_params)
	if @review.save
		redirect_to product_path(@product)
	else
		render 'new'
	end
end

private

def review_params
	params.require(:review).permit(:rating,:comment,:judul)
end

def find_product
	@product = Product.find(params[:product_id])
end

end
