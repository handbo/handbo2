class KomentarsController < ApplicationController
	def create
    @user = User.find(params[:user_id])
    @komentar = @user.komentars.create(koment_params)
    redirect_to user_path(@user)
  end
  def index
  	@komentars = Komentar.all
  end
 
  private
    def koment_params
      params.require(:koment).permit(:commenter, :body)
    end
end
