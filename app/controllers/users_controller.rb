class UsersController < ApplicationController
  #before_action :set_user, only: [:edit, :update, :destroy]
  before_action :logged_in_user,only:[:index,:edit,:update,:destroy,:show]
  #before_action :set_user,only: [:show]

  # GET /users
  # GET /users.json
  def index
    @user = current_user
    products = current_user.products
    @users = @user.products
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])
  end

  # GET /users/new
  def new
    @user = User.new
    @users = current_user
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  def myproduct
    @users = User.all
  end
   
  def sewa
    
   end 

   def booked
    @user = current_user.products
   end

   def history
    @products = current_user.products
     
   end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save            
        UserMailer.registration_confirmation(@user).deliver
        #UserMailer.welcome_email(@user).deliver_later
        format.html { redirect_to login_path,notice:"Silahkan Konfirmasi password dan email" }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def confirm_email
  user = User.find_by_confirm_token(params[:id])
   if user
      user.email_confirmed
      flash[:success] = 'Selamat Datang di Handbo, Akun anda telah aktif'
     redirect_to login_url
   else
     flash[:error] = "Sorry. User does not exist"
     redirect_to login_url
 end
end


  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @user = User.find(params[:id])
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation,:user_id,:current_user_id,:current_user,:item, :subitem,:image, :from, :until, :price, :description, :stadt, :image,:produktambahan,:klasifikasikategory,:nilaikualitas,:product_id,:nama,:nomor,:kota,:nohp)
    end

    def logged_in_user
    unless logged_in?
      flash[:danger] ="Please Log in"
      
    end    
  end

end

