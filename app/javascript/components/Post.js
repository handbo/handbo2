var React = require("react")
var Post = React.createClass({
  propTypes: {
    title: React.PropTypes.string
  },

  render: function() {
    return (
      <div>
        <div>Title: {this.props.title}</div>
      </div>
    );
  }
});
module.exports = Post
