# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


l = Location.create(name:'Bekasi')
l.recordings.create(temp:30)
l.recordings.create(temp:40)
l.recordings.create(temp:50)
l.recordings.create(temp:60)
l.recordings.create(temp:70)
