class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.string :nama
      t.integer :nomor
      t.string :tujuan
      t.string :keterangan

      t.timestamps null: false
    end
  end
end
