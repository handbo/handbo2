class AddHargaToProduct < ActiveRecord::Migration
  def change
  	add_column :products, :harga6jam, :decimal
  	add_column :products, :harga12jam, :decimal
  	add_column :products, :hargafullday, :decimal
  end
end
