class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.references :product


      t.timestamps null: false
    end
    add_index :assets, :product_id
  end
end
