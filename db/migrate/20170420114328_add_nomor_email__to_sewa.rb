class AddNomorEmailToSewa < ActiveRecord::Migration
  def change
    add_column :sewas, :email, :string
    add_column :sewas, :nomorhp, :string
    add_reference :sewas, :user, index: true, foreign_key: true
    add_reference :sewas, :product, index: true, foreign_key: true
  end
end
