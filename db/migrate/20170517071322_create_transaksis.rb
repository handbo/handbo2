class CreateTransaksis < ActiveRecord::Migration
  def change
    create_table :transaksis do |t|
      t.string :commenter
      t.text :body
      t.datetime :tanggal
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
