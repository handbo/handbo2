class CreatePenawarans < ActiveRecord::Migration
  def change
    create_table :penawarans do |t|
      t.string :name
      t.date :time
      t.references :user, index: true, foreign_key: true
      t.references :product, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
