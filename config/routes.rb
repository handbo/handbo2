require 'api_constraints'

Rails.application.routes.draw do

  

  get 'hello_world', to: 'hello_world#index'
  #resources :rekamen
  #resources :records
  #resources :records
  #resources :records
  resources :requests
  namespace :api do
    namespace :v1 do
      resources :locations do
        resources :recordings
        end
      end
    end


  get 'lift/index'

  resources :posts

  resources :sewas
  resources :events
 
  
  resources :password_resets, only: [:new, :create, :edit, :update]
  get 'password_resets/edit'

  get 'password_resets/new'

  
  resources :password_resets, only: [:new, :create, :edit, :update]
  get 'password_resets/edit
  '
  get 'password_resets/new'

  get 'blog/artikel3'
  get 'products/home'



  resources :products do 
    resources :sewas
    
  end

  resources :users do
    member do
      get :history
    end
  end

  resources :products do
    member do 
      get :booked
    end
  end

  resources :products do
    member do
      get :show
    end
  end
  
  resources :products do
    resources :reviews
  end
   
  resources :users do
    resources :komentars
  end

  resources :users do
    resources :products
  end
  resources :sessions
  get 'sessions/new'

  resources :products do
    resources :posts
  end

  resources :users do
    resources :sewas
  end

  resources :users do
    resources :transaksis
  end

  resources :products do
    collection do
      get 'booked'
    end
  end

  resources :products do
    member do
      get :booked
      patch :sewa
    end
  end

  
  get 'blog/index'
  get 'blog/artikel1'
  get 'blog/artikel2'

  get 'pages/home'

  get 'pages/blog'



  namespace :api,defaults: {format: :json}, 
    constraints: { subdomain: 'api' }, path: '/'  do
        scope module: :v1,constraints: ApiConstraints.new(version: 1, default: true) do
      end
  end
  



  resources :weight_classes
  resources :length_classes
  get 'blog/index'

  resources :categories
  get 'welcome/index'

  resources :posts
  get 'pages/home'

  get 'pages/blog'


  
  
  #resources :events
  #root  'events#index'
  



  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  #root 'pages#home'

  #root 'visitors#index'
  #root 'welcome#index'
  
 
  #root 'blog#index'

  root 'welcome#index'
  #root 'lift#index'
  #root 'application#home'
  #root 'blog#index'

  #root 'blog#index'

  get 'login' => 'sessions#new'
  post 'login' => 'sessions#new'
  get 'logout' => 'sessions#destroy'
  get 'register' => 'users#new'
  post 'register' => 'users#new'
  get 'createitem' => 'products#new'
  post 'createitem' => 'products#new'
  get 'home' => 'products#index'
  get 'blog' => 'pages#blog'
  get 'lihat' => 'products#lihat'
  post 'lihat' => 'products#lihat'
  get 'coba' => 'products#edit'
  post 'coba' => 'products#validasi'
  get 'transaksi' => 'products#penawaran'
  get 'myproduct' => 'users#index'
  get 'cara' => 'welcome#caramenyewa'
  
  get 'pesan' => 'users#sewa'
  
  
  get 'syaratdanketentuan' => 'welcome#syarat'
  get 'partnerdengankami' => 'welcome#partner'

  get '/products/:product_id/penawarans/:id' => 'penawarans#index'


  get 'products/:id/sewaitem' => 'products#edit',as: :'penawaran'

   resources :users do
    member do
      get :confirm_email
    end
  end



  
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view


  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  # 
  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     #   end
end
