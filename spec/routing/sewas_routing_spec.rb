require "rails_helper"

RSpec.describe SewasController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/sewas").to route_to("sewas#index")
    end

    it "routes to #new" do
      expect(:get => "/sewas/new").to route_to("sewas#new")
    end

    it "routes to #show" do
      expect(:get => "/sewas/1").to route_to("sewas#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/sewas/1/edit").to route_to("sewas#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/sewas").to route_to("sewas#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/sewas/1").to route_to("sewas#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/sewas/1").to route_to("sewas#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/sewas/1").to route_to("sewas#destroy", :id => "1")
    end

  end
end
