require 'rails_helper'

RSpec.describe "sewas/new", type: :view do
  before(:each) do
    assign(:sewa, Sewa.new(
      :user => nil,
      :product => nil
    ))
  end

  it "renders new sewa form" do
    render

    assert_select "form[action=?][method=?]", sewas_path, "post" do

      assert_select "input#sewa_user_id[name=?]", "sewa[user_id]"

      assert_select "input#sewa_product_id[name=?]", "sewa[product_id]"
    end
  end
end
