require 'rails_helper'

RSpec.describe "sewas/edit", type: :view do
  before(:each) do
    @sewa = assign(:sewa, Sewa.create!(
      :user => nil,
      :product => nil
    ))
  end

  it "renders the edit sewa form" do
    render

    assert_select "form[action=?][method=?]", sewa_path(@sewa), "post" do

      assert_select "input#sewa_user_id[name=?]", "sewa[user_id]"

      assert_select "input#sewa_product_id[name=?]", "sewa[product_id]"
    end
  end
end
